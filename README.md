# find-number and fizz-buzz problems from DATI IndProg course

## find-number
#### Problem statement:
1. You have sequence from 0 to M.
1. Input – unordered array of integer numbers of size M-n 3. Output – array of missing numbers in input array
1. Example: M = 5, arraySize = 3, array = {4, 1, 3} Output = {0, 2}

## fizz-buzz
#### Problem statement:
1. Input - sequence of numbers.
1. If number is divisible by 3, replace it with fizz
1. if number is divisible by 5, replace it with buzz
1. If both, then fizzbuzz
1. Output to the console
1. IMPORTANT: division operation and modulo operation is prohibited
## Note
1. Repository – “find-number/solution.cpp”, “fizz-buzz/solution.cpp” ONLY one file.
1. README.md – what is in repo and how to build
1. build.sh – build script
1. Comments to the functions – arguments and short description
1. Logic not in main(). Main is used only to cin/cout.
1. Inputs from cin. (for find-number N M and numbers, for fizz-buzz N and numbers)
1. If overflow long long cout error and return 1
1. If not a number cout error and return 1
1. Do not use external frameworks (only stl)

## build & run
### build:
`$ ./build.sh`
### run:
`$ ./build/find-number`\
`> [N] [M] [M - N array values]`

`$ ./build/fizz-buzz`\
`> [N] [N input numbers]`