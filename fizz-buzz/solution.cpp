/**
 * @file fizzbuzz.cpp
 * @author Iakov Gazizov (gazizov.yai@phystech.edu)
 * 
 * @brief "FIZZ BUZZ" problem from Industrial Programming course
 * 
 * #### Problem statement:
 * 1. Input - sequence of numbers.
 * 2. If number is divisible by 3, replace it with fizz
 * 3. if number is divisible by 5, replace it with buzz
 * 4. If both, then fizzbuzz
 * 5. Output to the console
 * 6. IMPORTANT: division operation and modulo operation is prohibited
 * 
 * #### Compilation options:
 * Define macro `OMITMAIN` to exclude main from compile routine
 * 
 * @date 2020-02-14
 * @copyright Copyright (c) 2020
 */

#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>
#include <exception>

/**
 * @brief number type constants
 */
enum class type_t: int
{
    NONE = 0,       ///< number is not divisible by 3 or 5
    FIZZ = 3,       ///< number is divisible by 3
    BUZZ = 5,       ///< number is divisible by 5
    FIZZBUZZ = 15   ///< number is divisible by 3 and 5
};

/**
 * @brief Skip unused symbols and validate std::string number for divisible checking
 * 
 * @param[in] number std::string number representation
 * 
 * @return std::string clear valid std::string number representation
 */
std::string getnumber(std::string number)
{
    size_t pos = 0;
    long long n = std::stoll(number, &pos);

    if(pos != number.length()) {
        throw std::invalid_argument("long long overflow/underfow");
    }

    return std::to_string(std::abs(n));
}

/**
 * @brief Check if number is in `buzz` type (see file description)
 * 
 * @param[in] number clear valid std::string number representation (UB otherwise)
 * 
 * @return bool check result
 */
bool isbuzz(std::string number)
{
    char last = *(std::end(number) - 1);
    return last == '0' || last == '5';
}

/**
 * @brief Check if number is in `fizz` type (see file description)
 * 
 * @param[in]   clear valid std::string number representation (UB otherwise)
 * 
 * @return bool check result
 */
bool isfizz(std::string number)
{
    int sum = 0;
    for(auto i = 0u; i < number.size(); ++i) {
        sum += number[i] - '0';
    }

    if(sum == 0 || sum == 3 || sum == 6 || sum == 9) {
        return true;
    } else if(sum >= 10) {
        return isfizz(std::to_string(sum));
    } else {
        return false;
    }
}

/**
 * @brief Find out std::string number type (see type_t)
 * 
 * @param[in]   clear valid std::string number representation (UB otherwise)
 * 
 * @return type_t number type
 */
type_t gettype(std::string number)
{
    bool resfizz = isfizz(number);
    bool resbuzz = isbuzz(number);

    if(resfizz && resbuzz) {
        return type_t::FIZZBUZZ;
    } else if(resfizz) {
        return type_t::FIZZ;
    } else if(resbuzz) {
        return type_t::BUZZ;
    } else {
        return type_t::NONE;
    }
}

/**
 * @brief "FIZZ BUZZ" implementation (see file description)
 * 
 * @param[in] numbers valid std::string input numbers (UB otherwise)
 * 
 * @return std::string FIZZ BUZZ result (with `"foo"` for none-type numbers)
 */
std::string fizzbuzz(const std::vector<std::string>& numbers)
{
    std::stringstream out;
    for(auto i = 0u; i < numbers.size(); ++i) {
        auto number = getnumber(numbers[i]);
        bool space = true;
        switch (gettype(number))
        {
            case type_t::FIZZ: {
                out << "fizz";
                break;
            }
            case type_t::BUZZ: {
                out << "buzz";
                break;
            }
            case type_t::FIZZBUZZ: {
                out << "fizzbuzz";
                break;
            }
            case type_t::NONE: {
                out << numbers[i];
                //space = false;
                break;
            };
        }
        if(space) {
            out << " ";
        }
    }

    return out.str();
}

#ifndef OMITMAIN

int main()
{
    int N = 0;
    std::vector<std::string> numbers;

    std::cin >> N;
    numbers.resize(N);
    for(auto i = 0; i < N; ++i) {
        std::cin >> numbers[i];
    }

    try {
        std::cout << fizzbuzz(numbers) << std::endl;
    } catch(...) {
        std::cerr << "error" << std::endl;
        exit(1);
    }

    return 0;
}

#endif // OMITMAIN
