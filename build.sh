#!/bin/sh

BUILD_DIR="./build"
FN_DIR="./find-number"
FB_DIR="./fizz-buzz"

CXX_COMPILER_FLAGS="-Wall -Wextra -pedantic -std=c++17"

# Parse arguments.
if [ $# -eq 1 ] && [ $1 = "clean" ]; then
    rm -r $BUILD_DIR
    exit 0
fi

# Make sure the build directory exist.
if [ ! -d $BUILD_DIR ]; then
  mkdir $BUILD_DIR
fi

c++ $CXX_COMPILER_FLAGS $FN_DIR/solution.cpp -o $BUILD_DIR/find-number
c++ $CXX_COMPILER_FLAGS $FB_DIR/solution.cpp -o $BUILD_DIR/fizz-buzz
