/**
 * @file findnumber.cpp
 * @author Iakov Gazizov (gazizov.yai@phystech.edu)
 * 
 * @brief "Find number" problem from Industrial Programming course
 * 
 * #### Problem statement:
 * 1. You have sequence from 0 to M.
 * 1. Input – unordered array of integer numbers of size M-n 3. Output – array of missing numbers in input array
 * 1. Example: M = 5, arraySize = 3, array = {4, 1, 3} Output = {0, 2}
 * 
 * #### Compilation options:
 * Define macro `OMITMAIN` to exclude main from compile routine
 * 
 * @date 2020-02-14
 * @copyright Copyright (c) 2020
 */

#include <iostream>
#include <algorithm>
#include <vector>

/**
 * @brief "Find Number" implementation (see file description)
 * 
 * @param[out]  output      std::vector to store found missing numbers array
 * @param[in]   array       std::vector of input numbers (must be sorted)
 * @param[in]   arraySize   array.size()
 * @param[in]   M           whole sequence size
 * 
 * @return int amount of found numbers {-1 on error}
 */
int findnumber(std::vector<int>& output, const std::vector<int>& array, int arraySize, int M)
{
    if(arraySize > M || arraySize < 0 || M < 0) {
        output.clear();
        return -1;
    }

    int outputSize = M - arraySize;
    output.resize(outputSize);

    int ii = 0; // input array index
    int io = 0; // output array index

    for(int val = 0; val < M; ++val) {
        if(ii < arraySize && array[ii] == val) {
            ii++;
        } else {
            output[io] = val;
            io++;
        }
    }

    return outputSize;
}

#ifndef OMITMAIN

int main()
{
    int N = 0;
    int M = 0;
    std::vector<int> array;

    std::cin >> M >> N;
    array.resize(M - N);
    for(int i = 0; i < M - N; ++i) {
        std::cin >> array[i];
    }
    std::sort(std::begin(array), std::end(array));

    std::vector<int> output;
    findnumber(output, array, M - N, M);

    for(auto &val: output) {
        std::cout << val << " ";
    }
    std::cout << std::endl;

    return 0;
}

#endif // OMITMAIN
