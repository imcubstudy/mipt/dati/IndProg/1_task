/**
 * @file fizzbuzz_test.cpp
 * @author Iakov Gazizov (gazizov.yai@phystech.edu)
 * 
 * @brief Test for "FIZZ BUZZ" problem from Industrial Programming course
 * 
 * @date 2020-02-14
 * @copyright Copyright (c) 2020
 */

#define OMITMAIN
#   include "fizzbuzz.cpp"
#undef OMITMAIN

struct output_t
{
    std::string output;
};

struct test_t
{
    std::vector<std::string> numbers;
    output_t expected;
};

bool dotest(const test_t& test)
{
    output_t output;
    output.output = fizzbuzz(test.numbers);
    return output.output == test.expected.output;
}

int main()
{
    std::vector<test_t> tests = {
        test_t {
            .numbers = {},
            .expected = output_t {
                .output = ""
            }
        },
        test_t {
            .numbers = {"0"},
            .expected = output_t {
                .output = "fizzbuzz "
            }
        },
        test_t {
            .numbers = {"3"},
            .expected = output_t {
                .output = "fizz "
            }
        },
        test_t {
            .numbers = {"5"},
            .expected = output_t {
                .output = "buzz "
            }
        },
        test_t {
            .numbers = {"15"},
            .expected = output_t {
                .output = "fizzbuzz "
            }
        },
        test_t {
            .numbers = {"16"},
            .expected = output_t {
                .output = "foo "
            }
        },
        test_t {
            .numbers = {"0", "1", "3", "5", "15", "16"},
            .expected = output_t {
                .output = "fizzbuzz foo fizz buzz fizzbuzz foo "
            }
        }
    };

    int test_id = 0;
    for(auto& t: tests) {
        if(dotest(t)) {
            std::cout << test_id++ << " PASSED" << std::endl;
        } else {
            std::cout << test_id++ << " FAILED" << std::endl;
        }
    }

    return 0;
}
