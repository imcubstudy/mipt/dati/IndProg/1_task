/**
 * @file findnumber_test.cpp
 * @author Iakov Gazizov (gazizov.yai@phystech.edu)
 * 
 * @brief Test for "Find number" problem from Industrial Programming course
 * 
 * @date 2020-02-14
 * @copyright Copyright (c) 2020
 */

#define OMITMAIN
#   include "findnumber.cpp"
#undef OMITMAIN

bool operator==(std::vector<int>& lhs, std::vector<int>& rhs)
{
    if(lhs.size() != rhs.size()) {
        return false;
    }

    std::sort(std::begin(lhs), std::end(lhs));
    std::sort(std::begin(rhs), std::end(rhs));

    for(auto i = 0u; i < lhs.size(); ++i) {
        if(lhs[i] != rhs[i]) {
            return false;
        }
    }

    return true;
}

struct output_t
{
    int outputSize;
    std::vector<int> output;
};

struct test_t
{
    std::vector<int> array;
    int arraySize;
    int M;
    output_t expected;
};

bool dotest(const test_t& test)
{
    output_t output;
    output.outputSize = findnumber(output.output, test.array, test.arraySize, test.M);

    if(output.outputSize == test.expected.outputSize && 
       output.output     == test.expected.output) {
        return true;
    } else {
        return false;
    }
}

int main()
{
    std::vector<test_t> tests = {
        test_t {
            .array = {1, 3, 4},
            .arraySize = 3,
            .M = 5,
            .expected = output_t {
                .outputSize = 2,
                .output = {0, 2}
            }
        },
        test_t {
            .array = {0, 1, 2, 3, 4},
            .arraySize = 5,
            .M = 5,
            .expected = output_t {
                .outputSize = 0,
                .output = {}
            }
        },
        test_t {
            .array = {},
            .arraySize = 0,
            .M = 5,
            .expected = output_t {
                .outputSize = 5,
                .output = {0, 1, 2, 3, 4}
            }
        },
        test_t {
            .array = {},
            .arraySize = -1,
            .M = 5,
            .expected = output_t {
                .outputSize = -1,
                .output = {}
            }
        },
        test_t {
            .array = {},
            .arraySize = 1,
            .M = -5,
            .expected = output_t {
                .outputSize = -1,
                .output = {}
            }
        },
        test_t {
            .array = {},
            .arraySize = 6,
            .M = 5,
            .expected = output_t {
                .outputSize = -1,
                .output = {}
            }
        }
    };

    int test_id = 0;
    for(auto& t: tests) {
        if(dotest(t)) {
            std::cout << test_id++ << " PASSED" << std::endl;
        } else {
            std::cout << test_id++ << " FAILED" << std::endl;
        }
    }

    return 0;
}
